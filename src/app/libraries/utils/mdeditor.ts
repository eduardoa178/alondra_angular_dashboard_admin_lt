import { Injectable } from '@angular/core';
import { AfterViewInit,OnInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
const SimpleMDE: any = require('simplemde');

@Component({
  selector: 'app-mdeditor',
  templateUrl: './mdeditor.html',
})
export class MDEditorComponent implements AfterViewInit {
  @Input() model: string;
  @Output() modelChange = new EventEmitter<string>();
  @ViewChild('simplemde') textarea: ElementRef;

  constructor(private elementRef: ElementRef) { }

  ngAfterViewInit() {
    var modelChange = this.modelChange;
    var mde = new SimpleMDE({
      element: this.textarea.nativeElement,
      forceSync: true,
       spellChecker: false,
      status: true
    });

    mde.codemirror.on('change', function() {
      var value = mde.codemirror.getValue();
      modelChange.emit(value);
    });
   
    if (this.model) {
      mde.codemirror.setValue(this.model);
    }
  }
}
