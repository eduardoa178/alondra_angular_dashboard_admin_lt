import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

declare var getEncriptedDomain:any;
@Injectable()
export class HttpJsonHeaderOptionsService {
  constructor() {}
  get_auth_header (token:string){
    let headers = new HttpHeaders({ Authorization:'Token ' + token});
    headers.set('Content-Type','application/json');
    return headers;
  }
  get_auth_params_option (token:string,body:any){
    const headers = new HttpHeaders({ Authorization:'Token ' + token});
    headers.set('Content-Type','application/json');
    const httpOptions = {headers:headers,body:body};
    return httpOptions;
  }
  get_auth (token:string){
    const headers = new HttpHeaders({ Authorization:'Token ' + token});
    headers.set('Content-Type','application/json');
    const httpOptions = {headers:headers};
    return httpOptions;
  }
  get_auth_params (token:string,params:any){
    const headers = new HttpHeaders({ Authorization:'Token ' + token});
    headers.set('Content-Type','application/json');
    const httpOptions = {headers:headers,params:params};
    return httpOptions;
  }
  getDefault (){

    const headers = new HttpHeaders({ });
    headers.set('Content-Type','application/json');
    const httpOptions = {headers:headers};
    return httpOptions;
  }
}