import { Component, OnInit } from '@angular/core';
import { Data } from '.././category'
import { CategoryService } from '../../categories/category.service'
import { Router,ActivatedRoute } from '@angular/router';
import { CustomMethodsService } from '../../../libraries/utils/custommethods.service';
import { AdviableVar } from '../../../libraries/utils/customvars';
import { Globals } from "../../main/globals";

@Component({
  selector: 'app-categories_edit',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesEditComponent implements OnInit {
  model:Data;
  constructor(  
    private custom_methods:CustomMethodsService,
    private router: Router,
    private route:ActivatedRoute, 
    private categories:CategoryService,
    private global:Globals) { }


  ngOnInit() {
      this.global.initializeActions();
      this.initialize();
      this.route.params.subscribe(params =>{

        if (typeof params['id'] !== 'undefined') 
        {       
          
          this.categories.Get(params['id']).subscribe((data:Data) => {
          this.model = data;
          this.global.actions.is_new = false;
        });
        }
        
      });
    
  }
  initialize()
  {
    this.model =  {
      name:"",
      meta_title:"",
      meta_description:'',
      slug:"",
      id:null,
      publish:true,
      checked:false,
      selected:false,
    };
  }
  save()
  {  
      var that = this;
      var disable_updated_box = function() {
        that.global.actions.updated = false;
      }
      if (this.global.actions.is_new == false)
      {
        this.categories.Update(this.model).subscribe((data:any) => {

          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }else
      {
        this.categories.New(this.model).subscribe((data:Data) => {
          this.model = data;   
          this.global.actions.is_new = false;   
          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }
    
     
    
  }
  FindSlug()
  {
    this.categories.FindSlug(this.model.slug).subscribe((data:AdviableVar) => {

      this.global.actions.exists_slug = data;
    });
  }
  FindTitle()
  {
    this.categories.Find(this.model.name).subscribe((data:AdviableVar) => {

       this.global.actions.exists_title = data;
       this.global.actions.exists_slug = data;

    });
  }
  TitleChanged()
  {
    
    this.model.slug = this.custom_methods.string_to_slug(this.model.name);
    this.model.meta_title = this.model.name;
  }
  back()
  {
     this.router.navigate(['/categories' ]);
  }
  DELETE()
  {
   
    this.categories.Delete(this.model.id).subscribe((data:any) => {
      this.router.navigate(['/categories' ]);

    });
    
  }

}
