import { Component, OnInit } from '@angular/core';
import { Data } from '.././tags'
import { TagsService } from '../../tags/tags.service'
import { Router,ActivatedRoute } from '@angular/router';
import { CustomMethodsService } from '../../../libraries/utils/custommethods.service';
import { AdviableVar } from '../../../libraries/utils/customvars';
import { Globals } from "../../main/globals";

@Component({
  selector: 'app-tags_edit',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})
export class TagsEditComponent implements OnInit {
  model:Data;
  
  constructor(  
    private custom_methods:CustomMethodsService,
    private router: Router,
    private route:ActivatedRoute, 
    private tags:TagsService,
    private global:Globals) { }


  ngOnInit() {
      this.global.initializeActions();
      this.initialize();
      this.route.params.subscribe(params =>{

        if (typeof params['id'] !== 'undefined') 
        {       
          
          this.tags.Get(params['id']).subscribe((data:Data) => {
          this.model = data;
          this.global.actions.is_new = false;
        });
        }
        
      });
    
  }
  initialize()
  {

    const data = {
      name:"",
      meta_title:"",
      meta_description:'',
      slug:"",
      id:null,
      publish:true,
      checked:false,
      selected:false
    };
    this.model = data;
  }
  save()
  {  
      var that = this;
      var disable_updated_box = function() {
        that.global.actions.updated = false;
      }
      if (this.global.actions.is_new == false)
      {
        this.tags.Update(this.model).subscribe((data:any) => {

          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }else
      {
        this.tags.New(this.model).subscribe((data:Data) => {
          this.model = data;   
          this.global.actions.is_new = false;   
          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }
    
     
    
  }
  FindSlug()
  {
    this.tags.FindSlug(this.model.slug).subscribe((data:AdviableVar) => {

      this.global.actions.exists_slug = data;
    });
  }
  FindTitle()
  {
    this.tags.Find(this.model.name).subscribe((data:AdviableVar) => {

       this.global.actions.exists_title = data;
       this.global.actions.exists_slug = data;

    });
  }
  TitleChanged()
  {
    
    this.model.slug = this.custom_methods.string_to_slug(this.model.name);
    this.model.meta_title = this.model.name;
  }
  back()
  {
     this.router.navigate(['/tags' ]);
  }
  DELETE()
  {
   
    this.tags.Delete(this.model.id).subscribe((data:any) => {
      this.router.navigate(['/tags' ]);

    });
    
  }

}
