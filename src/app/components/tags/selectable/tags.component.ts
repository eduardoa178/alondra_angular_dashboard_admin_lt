import { Component, OnInit } from '@angular/core';
import { TagsService } from '../../tags/tags.service'
import { Data,Pagination } from '.././tags'
import { Router } from '@angular/router';
import { Globals } from "../../main/globals";
import { AfterViewInit, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
@Component({

  selector: 'app-tags_selectable',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})
export class TagsSelectableComponent implements AfterViewInit {
  page:number = 1;
  items: Data[];
  query:string = "";
  selected:boolean = false;
  selectedable:boolean = false;
  pages: number = 0;
  @Input() model: Data[];
  @Output() modelChange = new EventEmitter<Data[]>()
  constructor(
    private router: Router, 
    private tags:TagsService,
    private global:Globals,
    private elementRef: ElementRef,

    ) {
    this.model = []

 
 }
  ngAfterViewInit() {
    //inject dashboard
    this.global.initializeActions();

    this.tags.List(this.page).subscribe((data:Pagination) => {
      data.items.forEach(obj => {
          obj.checked = false;
          obj.selected = false;
          this.model.forEach(objx => {
            if (objx.id == obj.id)
            {
              obj.selected = true;
            }
            
          })
         })
      this.items = data.items ;
      this.pages = data.pages;
    });

  }
  loadmore()
  {
    this.page ++;
    if( this.page >= this.pages)
    {
      this.page = this.pages;
    }
    if(this.query.length >0 )
    {
        this.tags.Search(
          this.page,
          this.query
        ).subscribe((data:Pagination) => {
          data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        this.items.concat(data.items) ;
        this.pages = data.pages;
      });
    }else
    {
      this.tags.List(this.page).subscribe((data:Pagination) => {
        this.pages = data.pages;
        data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        this.items.concat(data.items) ;
      });
    }
   
  }
  SELECT_ALL(){
    if(this.selected == false)
    {
      
     this.items.forEach(obj => {
            obj.checked = true;

         })
     
   }else
   {

     this.items.forEach(obj => {
            obj.checked = false;
         })
   }
    
    

    
    

  }
  DELETED_ALL(){
    
      var that = this;
      var disable_updated_box = function() {
        that.global.actions.deleted = false;
        
      }

      this.items.filter(obj => obj.checked == true).forEach(item => {
        this.tags.Delete(item.id).subscribe((data:any) => {
          this.global.actions.deleted = true;
          this.items = this.items.filter(objx => objx !== item);
        });
      })

      setTimeout(disable_updated_box, 3000);
  
      
  }
  SELECTABLE_SELECT_ALL(){
    if(this.selectedable == false)
    {
      this.selectedable  = true;
     this.items.forEach(obj => {
            obj.selected = true;
            

         })
     this.modelChange.emit( this.items.filter(objx => objx.selected === true));
   }else
   {
      this.selectedable  = false;
      this.items.forEach(obj => {
            obj.selected = false;

            
      })
      this.modelChange.emit( this.items.filter(objx => objx.selected === true));
   }
  
   
      
  }
  SELECTABLE_SELECT(item:Data){
    if(item.selected == false)
    {
       item.selected = true;
      
    }else{
      item.selected = false;
    
    }
    this.modelChange.emit( this.items.filter(objx => objx.selected === true));
  }
  EDIT(item:Data){
    this.router.navigate(['/tags/edit/',item.id ]);
  }
  DELETE(item:Data){

       var that = this;
      var disable_updated_box = function() {
        that.global.actions.deleted = false;
      }
  this.global.actions.deleted = true;
          
    this.tags.Delete(item.id).subscribe((data:any) => {
      setTimeout(disable_updated_box, 3000);
      
    });
    this.items = this.items.filter(obj => obj !== item);
  }
  New(item){
     this.router.navigate(['/tags/new' ]);
  }
  find()
  {
 
  
    this.page = 1;

    if(this.query.length > 0 )
    {
        this.tags.Search(
          this.page,
          this.query
        ).subscribe((data:Pagination) => {

         this.items = data.items ;
          this.pages = data.pages;
      });
    }else
    {
      this.tags.List(this.page).subscribe((data:Pagination) => {

        this.items = data.items ;
         this.pages = data.pages;
      });
    }
  }
}
