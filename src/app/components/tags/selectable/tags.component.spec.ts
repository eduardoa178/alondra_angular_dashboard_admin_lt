import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriesSelectableComponent } from './categories.component';

describe('CategoriesSelectableComponent', () => {
  let component: CategoriesSelectableComponent;
  let fixture: ComponentFixture<CategoriesSelectableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoriesSelectableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriesSelectableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
