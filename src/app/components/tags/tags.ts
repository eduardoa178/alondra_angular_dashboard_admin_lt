
export interface Data {
	id: number;
	name: string;
	slug: string;
	meta_title: string;
	meta_description: string;
	publish: boolean;
	checked: boolean;
	selected:boolean;
}

export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}