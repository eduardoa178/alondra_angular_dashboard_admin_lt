import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { ActionsVars } from '../../libraries/utils/customvars';
@Injectable()
export class Globals {
    token: string = "";
    logged: boolean;
    actions:ActionsVars;
    

   constructor( private cookie:CookieService) 
   {
   	
    this.initializeToken();
    
  }
  public set_token(token:string,remember_me:boolean)
  {
  	this.token = token;
  	this.logged = true;
  	if(remember_me == true)
  	{
  		this.cookie.set('token', token);
  	}
  }
  public remove_token()
  {
  	this.token = '';
  	this.logged = false;
  	this.cookie.delete('token');
  	
  }

  public check_token(token:string)
  {
  	return this.token == token && this.cookie.get('token') == token;  	
  }

  public get_token()
  {
  	return this.cookie.get('token') !== "undefined";  	
  }

  private initializeToken()
  {
      if(this.get_token()==true)
    {
      this.logged = true;
      this.token = this.cookie.get('token');
    }else
    {
      this.logged = false;
    }
  }

  public initializeActions()
  {
    const actions = {
      is_new:true,
      updated:false,
      deleted:false,
      exists_slug:{adviable:true},
      exists_title:{adviable:true}
    };
    this.actions = actions;
  }


}