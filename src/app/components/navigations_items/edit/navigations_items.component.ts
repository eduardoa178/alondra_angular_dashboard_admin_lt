import { Component, OnInit } from '@angular/core';
import { Data2 } from '.././navigations_items'
import { NavigationsItemsService } from '../../navigations_items/navigations_items.service'
import { Router,ActivatedRoute } from '@angular/router';
import { CustomMethodsService } from '../../../libraries/utils/custommethods.service';
import { AdviableVar } from '../../../libraries/utils/customvars';
import { Globals } from "../../main/globals";
import { AfterViewInit, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
declare var jQuery:any;
declare var $:any;
@Component({
  selector: 'app-navigations_edit',
  templateUrl: './navigations_items.component.html',
  styleUrls: ['./navigations_items.component.css']
})
export class NavigationsItemsEditComponent implements OnInit {
   model:Data2;

   @Input() parent: number = 0;
  constructor(  
    private custom_methods:CustomMethodsService,
    private router: Router,
    private route:ActivatedRoute, 
    private navigations:NavigationsItemsService,
    private global:Globals) { }


  ngOnInit() {
      this.global.initializeActions();
      this.initialize();
      this.route.params.subscribe(params =>{
         if (typeof params['parent'] !== 'undefined') 
          {
            this.parent = params['parent']
          }
        if (typeof params['id'] !== 'undefined') 
        {       
          
          this.navigations.Get(params['id']).subscribe((data:Data2) => {
          this.model = data;
          this.global.actions.is_new = false;
        });
        }
        
      });
    
  }


  initialize()
  {
    this.model =  {
    
      id:null,
      link: "",
      image: "",
      title: "",
      parent: null,
      parent_id: null,
      checked:false,
      selected:false,
      position:null
    };
  }
  save()
  {  
      var that = this;
      this.model.position = this.parent;
      var disable_updated_box = function() {
        that.global.actions.updated = false;
      }
      if (this.global.actions.is_new == false)
      {
        this.navigations.Update(this.model).subscribe((data:any) => {

          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }else
      {
        this.navigations.New(this.model).subscribe((data:Data2) => {
          this.model = data;   
          this.global.actions.is_new = false;   
          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }
    
     
    
  }
  back()
  {
     this.router.navigate(['/navigations/edit',this.parent ]);
  }
  DELETE()
  {
   
    this.navigations.Delete(this.model.id).subscribe((data:any) => {
      this.router.navigate(['/navigations/edit',this.parent ]);

    });
    
  }

}
