import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationsItemsEditComponent } from './navigations_items.component';

describe('NavigationsEditComponent', () => {
  let component: NavigationsItemsEditComponent;
  let fixture: ComponentFixture<NavigationsItemsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationsItemsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationsItemsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
