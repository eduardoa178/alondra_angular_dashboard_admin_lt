

export interface Data2 {
	id: number;
	link: string;
	image: string;
	title: string;
	parent: Data2;
	parent_id: number;
	checked: boolean;
	selected: boolean;
	position:number;
}


export interface Data {
	id: number;
	link: string;
	image: string;
	title: string;
	parent: Data;
	parent_id: number;
	checked: boolean;
	selected: boolean;

}


export interface Pagination {
	pages: number;
	items: Data[];
	items2: any,
	next_page: string;
	prev_page: string;
}


