import { Component, OnInit } from '@angular/core';
import { Data2,Data,Pagination } from '.././navigations_items'
import {Data as dd2 } from '../../navigations/navigations'
import { NavigationsService } from "../../navigations/navigations.service"
import { NavigationsItemsService} from ".././navigations_items.service"
import { Router,ActivatedRoute } from '@angular/router';
import { CustomMethodsService } from '../../../libraries/utils/custommethods.service';
import { AdviableVar } from '../../../libraries/utils/customvars';
import { Globals } from "../../main/globals";
import { AfterViewInit, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
declare var jQuery:any;
declare var $:any;
@Component({
  selector: 'app-navigations_selectable',
  templateUrl: './navigations_items.component.html',
  styleUrls: ['./navigations_items.component.css']
})
export class NavigationsItemsSelectableComponent implements OnInit {

  model:Data2;
  page:number;
  items: Data[];
  query:string;
  selected:boolean;
  pages: number;
  @Input() model2: dd2;
  @Input() parent: number = 0;
  constructor(
    private router: Router, 
    private route:ActivatedRoute, 
    private nav:NavigationsService,
    private navigations:NavigationsItemsService,
    private global:Globals
    ) { }
  ngOnInit() {
    //inject dashboard
    this.global.initializeActions();
  
    this.page = 1;
    this.query = '';
    this.selected = false;
    this.navigations.List(this.page,this.parent).subscribe((data:Pagination) => {
      data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        
         this.items = data.items ;
       
       

       this.initializeEditor(this.items);
       this.pages = data.pages;
    });
    
  }
   initializeEditor(items)
  {
      jQuery("#nestable2").nestable({group: 1});   
      jQuery("#nestable3").nestable({group: 1});
        function  listify(strarr) {
          
          var l = $("<ol>").addClass("dd-empty");
            if (strarr.length>0)
            {
              l = $("<ol>").addClass("dd-list");
            }
            //arreglar esto a futuro
          jQuery.each(strarr, function(i, v) {
              var c = jQuery("<li>").addClass("dd-item"),
                h = jQuery("<div>").addClass("dd-handle").text(v["title"]);
              c.attr('data-id',v["id"])

              c.append(h);
              l.append(c.append(h));
              if (!!v["parent"] && v["parent"].length > 0)
              {            
                c.append(listify(v["parent"]));
              }

          });
          return l;
        }
      var that = this
      var d =  listify(items);
          
          if (d !== null)
          {
            jQuery("#nestable").append(d);
          }
      
        jQuery("#nestable").nestable({group: 1,maxDepth :7}); 
        $('.dd').on('change', function() {
            that.model2.navigation = jQuery("#nestable").nestable('serialize')
        
      
            ;
         });
  }
  
  save()
  {  
      var that = this;
      this.model.position = this.parent;
     
    
     
    
  }

}
