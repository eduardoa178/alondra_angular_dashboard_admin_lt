
export interface Data {
	id: number;
	comment:string,
	email:string,
	name:string,
	website:string,
	status:string,
	rating:number,
	checked: boolean;
	selected: boolean;
}


export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}