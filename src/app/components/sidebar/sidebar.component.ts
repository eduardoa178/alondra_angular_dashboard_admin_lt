import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
   selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  //styleUrls: ['./app.component.css']
})
export class SidebarComponent implements OnInit {
	title = 'sidebar';
	settings: boolean = true;
  router: Router;
  ngOnInit() 
  {

  }
  constructor( router: Router) 
  {
  	this.router = router;
  }

	Logoutx()
	{
  
		this.router.navigate(['/sidebar']);
	}
}
