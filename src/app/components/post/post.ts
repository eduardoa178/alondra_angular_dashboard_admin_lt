
import {Data as Category}  from '../categories/category'; 
import {Data as Tags}  from '../tags/tags'; 

export interface Data {
	id: number;
	title: string;
	slug: string;
	meta_title: string;
	meta_description: string;
	publish: boolean;
	checked: boolean;
	tag_lists:Tags[];
	tags_lists:Tags[];
	categories_lists:Category[];
	related_postsx:number[];
	releated_posts:number[];
	thumbnail: string;
	thumbnail_text: string;
	featured_image: string;
	featured_image_text: string;
	content: string;
	excerpt: string;	
	publish_date: string;
//	featured_start_date: string;
//	featured_end_date: string;
	is_featured: boolean;
	is_on_feed: boolean;
	selected:boolean;
}


export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}


