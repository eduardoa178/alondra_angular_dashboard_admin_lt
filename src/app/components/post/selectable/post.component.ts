import { Component, OnInit } from '@angular/core';
import { PostService } from '../../post/post.service'
import { Data,Pagination } from '.././post'
import { Router } from '@angular/router';
import { Globals } from "../../main/globals";
import { AfterViewInit, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
@Component({

  selector: 'app-posts_selectable',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostSelectableComponent implements AfterViewInit {

  results: number[] ;
  page:number = 1;
  items: Data[];
  query:string = "";
  selected:boolean = false;
  selectedable:boolean = false;
  pages: number = 0;
  @Input() model: number[];
  @Input() exclude: number;
  @Output() modelChange = new EventEmitter<number[]>()
  constructor(
    private router: Router, 
    private post:PostService,
    private global:Globals,
    private elementRef: ElementRef,

    ) {
    this.model = []

 
 }
  ngAfterViewInit() {
    //inject dashboard
    this.global.initializeActions();
    
    
    this.results = []
    this.post.List(this.page).subscribe((data:Pagination) => {
      data.items.filter(obj => obj.id != this.exclude).forEach(obj => {
          obj.checked = false;
          obj.selected = false;
          this.model.forEach(objx => {
            if (objx == obj.id)
            {
              obj.selected = true;
              this.results.push(obj.id)
            }
            
          })
         })
      this.items = data.items.filter(obj => obj.id != this.exclude);
      this.pages = data.pages;
    });

  }
  loadmore()
  {
    this.page ++;
    if( this.page >= this.pages)
    {
      this.page = this.pages;
    }
    if(this.query.length >0 )
    {
        this.post.Search(
          this.page,
          this.query
        ).subscribe((data:Pagination) => {
          data.items.filter(obj => obj.id == this.exclude).forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        this.items.concat(data.items.filter(obj => obj.id != this.exclude)) ;
        this.pages = data.pages;
      });
    }else
    {
      this.post.List(this.page).subscribe((data:Pagination) => {
        this.pages = data.pages;
        data.items.filter(obj => obj.id == this.exclude).forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        this.items.concat(data.items.filter(obj => obj.id != this.exclude)) ;
      });
    }
   
  }
  SELECT_ALL(){
    if(this.selected == false)
    {
      
     this.items.forEach(obj => {
            obj.checked = true;

         })
     
   }else
   {

     this.items.forEach(obj => {
            obj.checked = false;
         })
   }
    
    

    
    

  }
  DELETED_ALL(){
    
      var that = this;
      var disable_updated_box = function() {
        that.global.actions.deleted = false;
        
      }

      this.items.filter(obj => obj.checked == true).forEach(item => {
        this.post.Delete(item.id).subscribe((data:any) => {
          this.global.actions.deleted = true;
          this.items = this.items.filter(objx => objx !== item);
        });
      })

      setTimeout(disable_updated_box, 3000);
  
      
  }
  SELECTABLE_SELECT_ALL(){
    if(this.selectedable == false)
    {
      this.selectedable  = true;
     this.items.forEach(obj => {
            obj.selected = true;
            if(this.results.indexOf(obj.id) == -1)
            {
              this.results.push(obj.id)
              
            }
            this.modelChange.emit( this.results)
         })

    
    ;
   }else
   {
      this.selectedable  = false;
      this.items.forEach(obj => {
          obj.selected = false;
          this.results = this.results.filter(xs => xs !== obj.id)
          this.modelChange.emit( this.results)
            
      })
     
   }
      
  }
  SELECTABLE_SELECT(item:Data){
    if(item.selected == false)
    {
      item.selected = true;
      if(this.results.indexOf(item.id) == -1)
      {
        this.results.push(item.id)
        
      }
            
      this.modelChange.emit( this.results)
       
    }else{
      item.selected = false;
      this.results = this.results.filter(xs => xs !== item.id)
      this.modelChange.emit( this.results)
            
     
    }


            
  }
  EDIT(item:Data){
    this.router.navigate(['/post/edit/',item.id ]);
  }
  DELETE(item:Data){

       var that = this;
      var disable_updated_box = function() {
        that.global.actions.deleted = false;
      }
  this.global.actions.deleted = true;
          
    this.post.Delete(item.id).subscribe((data:any) => {
      setTimeout(disable_updated_box, 3000);
      
    });
    this.items = this.items.filter(obj => obj !== item);
  }
  New(item){
     this.router.navigate(['/post/new' ]);
  }
  find()
  {
 
  
    this.page = 1;

    if(this.query.length > 0 )
    {
        this.post.Search(
          this.page,
          this.query
        ).subscribe((data:Pagination) => {

         this.items = data.items ;
          this.pages = data.pages;
      });
    }else
    {
      this.post.List(this.page).subscribe((data:Pagination) => {

        this.items = data.items ;
         this.pages = data.pages;
      });
    }
  }
}
