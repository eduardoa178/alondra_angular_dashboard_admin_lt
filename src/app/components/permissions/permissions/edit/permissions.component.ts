import { Component, OnInit } from '@angular/core';
import { Data,Role,Permissions } from '.././permissions'
import { PermissionService } from '../../permissions/permissions.service'
import { Router,ActivatedRoute } from '@angular/router';
import { CustomMethodsService } from '../../../../libraries/utils/custommethods.service';
import { AdviableVar } from '../../../../libraries/utils/customvars';
import { Globals } from "../../../main/globals";

@Component({
  selector: 'app-permissions_edit',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.css']
})
export class PermissionsEditComponent implements OnInit {
  model:Data;
  roles:Role[];
  permissions:Role[];
  constructor(  
    private custom_methods:CustomMethodsService,
    private router: Router,
    private route:ActivatedRoute, 
    private categories:PermissionService,
    private global:Globals) { }


  ngOnInit() {
      this.global.initializeActions();
      this.initialize();
      this.route.params.subscribe(params =>{

        if (typeof params['id'] !== 'undefined') 
        {       
          
          this.categories.Get(params['id']).subscribe((data:Data) => {
          this.model = data;
          this.global.actions.is_new = false;
        });
        }
        
      });
    
  }
  initialize()
  {
    this.permissions = [
      {
        id:"posts",
        name:"POST_PLURAL_LABEL",
      },
       {
        id:"pages",
        name:"PAGES_PLURAL_LABEL",
      },
       {
        id:"permissions",
        name:"PERMISSIONS_LABEL",
      },
       {
        id:"category",
        name:"CATEGORY_LABEL",
      },

       {
        id:"tags",
        name:"TAGS_LABEL",
      },

       {
        id:"navigation",
        name:"NAVIGATION_PLURAL_LABEL",
      },

       {
        id:"media",
        name:"MEDIA_LABEL",
      },

       {
        id:"comments",
        name:"COMMENTS_LABEL",
      },


       {
        id:"user",
        name:"USER_PLURAL_LABEL",
      },


       {
        id:"settings",
        name:"SETTINGS_LABEL",
      },

    ];
    this.roles = [
   
      {
        id:"access",
        name:"access",
      },
      {
        id:"create",
        name:"create",
      },
      {
        id:"change",
        name:"change",
      },
          {
        id:"delete",
        name:"delete",
      },
    

    ]
    this.model =  {
      module: "",
      action: "",
      id:null,
      checked:false,
      selected:false,
    };
  }
  save()
  {  
      var that = this;
      var disable_updated_box = function() {
        that.global.actions.updated = false;
      }
      if (this.global.actions.is_new == false)
      {
        this.categories.Update(this.model).subscribe((data:any) => {

          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }else
      {
        this.categories.New(this.model).subscribe((data:Data) => {
          this.model = data;   
          this.global.actions.is_new = false;   
          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }
    
     
    
  }

  FindTitle()
  {
    this.categories.Find(this.model.module).subscribe((data:AdviableVar) => {

       this.global.actions.exists_title = data;
       this.global.actions.exists_slug = data;

    });
  }
 
  back()
  {
     this.router.navigate(['/permisions' ]);
  }
  DELETE()
  {
   
    this.categories.Delete(this.model.id).subscribe((data:any) => {
      this.router.navigate(['/permisions' ]);

    });
    
  }

}
