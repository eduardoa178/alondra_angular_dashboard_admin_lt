import { Component, OnInit } from '@angular/core';
import { Data } from '.././groups'
import { GroupService } from '../../groups/groups.service'
import { Router,ActivatedRoute } from '@angular/router';
import { CustomMethodsService } from '../../../../libraries/utils/custommethods.service';
import { AdviableVar } from '../../../../libraries/utils/customvars';
import { Globals } from "../../../main/globals";
import { Data as Permisions, Pagination as PermisionsPagination } from '../../permissions/permissions'
import { PermissionService } from '../../permissions/permissions.service'

@Component({
  selector: 'app-groups_edit',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsEditComponent implements OnInit {
  model:Data;
  permissions:Permisions[];
  page:number;
  pages: number;
  query:string;
  constructor(  
    private custom_methods:CustomMethodsService,
    private router: Router,
    private route:ActivatedRoute, 
    private groups:GroupService,
    private categories:PermissionService,
    private global:Globals) { }


  ngOnInit() {
      this.global.initializeActions();
      this.initialize();
      this.page = 1;
      this.pages = 0;
      this.query = '';
      this.route.params.subscribe(params =>{

      this.categories.List(1).subscribe((data:PermisionsPagination) => {
      
         this.permissions = data.items ;
         this.pages = data.pages;
          this.permissions.forEach(obj => {
            obj.selected = false;
          })
      });
        if (typeof params['id'] !== 'undefined') 
        {       
          
          this.groups.Get(params['id']).subscribe((data:Data) => {
          this.model = data;
          this.model.permission_lists.forEach(obj => {
            obj.selected = true;
            this.permissions.forEach(objx => {
                if(objx.id == obj.id)
                {
                  objx.selected = true;
                }
                
               
            })
          })

          this.global.actions.is_new = false;
        });
        }
        
      });
    
  }

  initialize()
  {
    this.permissions = [],
    this.model =  {
      name:"",
      permission_lists:null,
      id:null,
      checked:false,
      selected:false,
    };
  }
  save()
  {  
      var that = this;
      var disable_updated_box = function() {
        that.global.actions.updated = false;
      }
      if (this.global.actions.is_new == false)
      {
        this.groups.Update(this.model).subscribe((data:any) => {

          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }else
      {
        this.groups.New(this.model).subscribe((data:Data) => {
          this.model = data;   
          this.global.actions.is_new = false;   
          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }
    
     
    
  }
  itemSelected(item:Permisions)
  {
    return item.selected == true;
  }

 loadmore()
  {
    this.page ++;
    if( this.page >= this.pages)
    {
      this.page = this.pages;
    }
    if(this.query.length >0 )
    {
        this.categories.Search(
          this.page,
          this.query
        ).subscribe((data:PermisionsPagination) => {
          data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        this.permissions.concat(data.items) ;
        this.pages = data.pages;
      });
    }else
    {
      this.categories.List(this.page).subscribe((data:PermisionsPagination) => {
        this.pages = data.pages;
        data.items.forEach(obj => {
            obj.checked = false;
            obj.selected = false;
         })
        this.permissions.concat(data.items) ;
      });
    }
   
  }
  find()
  {
 
  
    this.page = 1;

    if(this.query.length > 0 )
    {
        this.categories.Search(
          this.page,
          this.query
        ).subscribe((data:PermisionsPagination) => {

         this.permissions = data.items ;
          this.pages = data.pages;
           this.permissions.forEach(obj => {
            obj.selected = false;
          })
          this.model.permission_lists.forEach(obj => {
            obj.selected = true;
            this.permissions.forEach(objx => {
                if(objx.id == obj.id)
                {
                  objx.selected = true;
                }
                
               
            })
          })
      });
    }else
    {
      this.categories.List(this.page).subscribe((data:PermisionsPagination) => {

        this.permissions = data.items ;
         this.pages = data.pages;
          this.permissions.forEach(obj => {
            obj.selected = false;
          })
          this.model.permission_lists.forEach(obj => {
            obj.selected = true;
            this.permissions.forEach(objx => {
                if(objx.id == obj.id)
                {
                  objx.selected = true;
                }
                
               
            })
          })
      });
    }
  }
  permissionselected(item:Permisions)
  {
    
    return item.selected == true
  }
  FindTitle()
  {
    this.groups.Find(this.model.name).subscribe((data:AdviableVar) => {

       this.global.actions.exists_title = data;
       this.global.actions.exists_slug = data;

    });
  }
 
  back()
  {
     this.router.navigate(['/permisions/groups' ]);
  }
  DELETE()
  {
   
    this.groups.Delete(this.model.id).subscribe((data:any) => {
      this.router.navigate(['/permisions/groups' ]);

    });
    
  }
  SELECT_COUNTRY(item:Permisions)
  {
    if(item.selected == false )
    {
      item.selected = true; 
      this.model.permission_lists.push(item)
    }else{
      item.selected = false; 
      this.model.permission_lists = this.model.permission_lists.filter(objx => objx.id !== item.id);
    }
   
  }
}
