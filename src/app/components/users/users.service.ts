import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import { RequestOptions, Request, RequestMethod} from '@angular/http';
import { environment } from '../../../environments/environment';
import { Globals } from "../main/globals";
import { CookieService } from 'ngx-cookie-service';
import { HttpJsonHeaderOptionsService } from '../../libraries/utils/httpoptions.service';
import { Data,Pagination } from './users';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})


export class UsersService {

  

  
  constructor(
    private http: HttpClient, 
    private cookie:CookieService,
    private global:Globals, 
    private header: HttpJsonHeaderOptionsService) { }


  me(page) {
    return this.http
      .get( 
        environment.url + "me/",
        httpOptions
      );
  }

  List(page) {
    const postedData = {page:page}
    return this.http
      .post( 
        environment.url + "users/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  Search(page, query) {
    const postedData = {
      page:page,
      query:query     
    }
    return this.http
      .post( 
        environment.url + "users/search/",
        postedData,
         this.header.get_auth(this.global.token)
      );
  }

  New(postedData) {
    return this.http
      .post( 
        environment.url + "new/user/",
        postedData,
         this.header.get_auth(this.global.token)
      );
  }

  Get(id) {
    const postedData = {
      id:id
    }    
    return this.http
      .post( 
        environment.url + "user/",
        postedData,
         this.header.get_auth(this.global.token)
      );
  }

  Update(postedData) {
    return this.http
      .put( 
        environment.url + "user/",
        postedData,
         this.header.get_auth(this.global.token)
      );
  }

  Delete(id) {
     const postedData = {
      id:id
    }   
    return this.http
      .put( 
        environment.url + "user/delete/",
        postedData,
         this.header.get_auth(this.global.token)
      );
  }

  UpdateMePassword(postedData){
    return this.http
      .put( 
        environment.url + "me/password/",
        postedData,
         this.header.get_auth(this.global.token)
      );
  }


}
