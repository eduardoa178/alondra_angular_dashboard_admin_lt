export interface Data {
	id: number;
	role: string,
	first_name: string,
	last_name: string,
	is_active:boolean,
	is_superuser:boolean,
	username: string,
	email: string,
	nick: string,
	checked:boolean;
}


export interface Role {
	id: string;
	name:string;
}
export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}