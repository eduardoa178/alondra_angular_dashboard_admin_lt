import { Component, OnInit } from '@angular/core';
import { PageService } from '../../page/page.service'
import { Data,Pagination } from '.././page'
import { Router } from '@angular/router';
import { Globals } from "../../main/globals";
@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {
  page:number;
  items: Data[];
  query:string;
  selected:boolean;
  pages: number;
  constructor(
    private router: Router, 
    private posts:PageService,
    private global:Globals
    ) { }
  ngOnInit() {
    //inject dashboard
    this.global.initializeActions();
    this.page = 1;
    this.query = '';
    this.selected = false;
    this.posts.List(this.page).subscribe((data:Pagination) => {
      data.items.forEach(obj => {
            obj.checked = false;
         })
       this.items = data.items ;
       this.pages = data.pages;
    });
    
  }
  loadmore()
  {
    this.page ++;
    if( this.page >= this.pages)
    {
      this.page = this.pages;
    }
    if(this.query.length >0 )
    {
        this.posts.Search(
          this.page,
          this.query
        ).subscribe((data:Pagination) => {
          data.items.forEach(obj => {
            obj.checked = false;
         })
        this.items.concat(data.items) ;
        this.pages = data.pages;
      });
    }else
    {
      this.posts.List(this.page).subscribe((data:Pagination) => {
        this.pages = data.pages;
        data.items.forEach(obj => {
            obj.checked = false;
         })
        this.items.concat(data.items) ;
      });
    }
   
  }
  SELECT_ALL(){
    if(this.selected == false)
    {
      
     this.items.forEach(obj => {
            obj.checked = true;
         })
   }else
   {

     this.items.forEach(obj => {
            obj.checked = false;
         })
   }
      
  }
  DELETED_ALL(){
    
      var that = this;
      var disable_updated_box = function() {
        that.global.actions.deleted = false;
      }

      this.items.filter(obj => obj.checked == true).forEach(item => {
        this.posts.Delete(item.id).subscribe((data:any) => {
          this.global.actions.deleted = true;
          this.items = this.items.filter(objx => objx !== item);
        });
      })

      setTimeout(disable_updated_box, 3000);
  
      
  }
  EDIT(item:Data){
    this.router.navigate(['/pages/edit/',item.id ]);
  }
  DELETE(item:Data){

       var that = this;
      var disable_updated_box = function() {
        that.global.actions.deleted = false;
      }
  this.global.actions.deleted = true;
          
    this.posts.Delete(item.id).subscribe((data:any) => {
      setTimeout(disable_updated_box, 3000);
      
    });
    this.items = this.items.filter(obj => obj !== item);
  }
  New(item){
     this.router.navigate(['/pages/new' ]);
  }
  find()
  {
 
  
    this.page = 1;

    if(this.query.length > 0 )
    {
        this.posts.Search(
          this.page,
          this.query
        ).subscribe((data:Pagination) => {

         this.items = data.items ;
          this.pages = data.pages;
      });
    }else
    {
      this.posts.List(this.page).subscribe((data:Pagination) => {

        this.items = data.items ;
         this.pages = data.pages;
      });
    }
  }
}
