export interface Data {
	id: number;
	title: string;
	slug: string;
	meta_title: string;
	meta_description: string;
	publish: boolean;
	checked: boolean;
	content: string;
	post_type:string;	
}


export interface Pagination {
	pages: number;
	items: Data[];
	next_page: string;
	prev_page: string;
}