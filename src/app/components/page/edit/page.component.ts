import { Component, OnInit } from '@angular/core';
import { Data } from '.././page'
import { PageService } from '../../page/page.service'
import { Router,ActivatedRoute } from '@angular/router';
import { CustomMethodsService } from '../../../libraries/utils/custommethods.service';
import { AdviableVar } from '../../../libraries/utils/customvars';
import { Globals } from "../../main/globals";
import { ElementRef,  ViewChild } from '@angular/core';
const SimpleMDE: any = require('simplemde');

@Component({
  selector: 'app-page_edit',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageEditComponent implements OnInit {
  model:Data;
    mde:any;
    @ViewChild('simplemde') textarea: ElementRef
  constructor(  
    private custom_methods:CustomMethodsService,
    private router: Router,
    private route:ActivatedRoute, 
    private page:PageService,
    private global:Globals) {
     
     }


  ngOnInit() {
      this.global.initializeActions();
       this.initialize();
       this.initializeEditor();
      this.route.params.subscribe(params =>{

        if (typeof params['id'] !== 'undefined') 
        {       
          
          this.page.Get(params['id']).subscribe((data:Data) => {
          this.model = data;
          this.mde.codemirror.setValue(this.model.content);
          this.global.actions.is_new = false;
        });
        }
        
      });
    
  }

  initialize()
  {
    this.model = {
      title:"",
      meta_title:"",
      meta_description:'',
      slug:"",
      content:'',
      id:null,
      post_type:'page',
      publish:true,
      checked:false,
    };
  }
   setContent(value:string)
  {
    this.model.content = value;
  }
   initializeEditor()
  {
 
    var mde = new SimpleMDE({
      element: this.textarea.nativeElement,
      forceSync: true,
      spellChecker: false,
      status: true
    });
    
     //pass the current instance
    var setValue = this;
    var ChangeEditor = function()
    {

      var value = mde.codemirror.getValue();
    
      setValue.setContent(value) ;  
      
    }
    mde.codemirror.on('change',ChangeEditor);
 
    this.mde = mde;

  }
  save()
  {  
      var that = this;
      var disable_updated_box = function() {
        that.global.actions.updated = false;
      }
      if (this.global.actions.is_new == false)
      {
        this.page.Update(this.model).subscribe((data:any) => {

          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }else
      {
        this.page.New(this.model).subscribe((data:Data) => {
          this.model = data;   
          this.global.actions.is_new = false;   
          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }
    
     
    
  }
  FindSlug()
  {
    this.page.FindSlug(this.model.slug).subscribe((data:AdviableVar) => {

      this.global.actions.exists_slug = data;
    });
  }
  FindTitle()
  {
    this.page.Find(this.model.title).subscribe((data:AdviableVar) => {

       this.global.actions.exists_title = data;
       this.global.actions.exists_slug = data;

    });
  }
  TitleChanged()
  {
    
    this.model.slug = this.custom_methods.string_to_slug(this.model.title);
    this.model.meta_title = this.model.title;
  }
  back()
  {
     this.router.navigate(['/pages' ]);
  }
  DELETE()
  {
   
    this.page.Delete(this.model.id).subscribe((data:any) => {
      this.router.navigate(['/pages' ]);

    });
    
  }

}
