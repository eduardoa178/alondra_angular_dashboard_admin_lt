import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Globals } from "../main/globals";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  
  constructor(private http: HttpClient) { }


  UpdateSettings(postedData) {
    return this.http
      .post( 
        environment.url + "settings/",
        postedData,
        httpOptions
      );
  }

  GetSettings(id) {
    return this.http
      .get( 
        environment.url + "settings/",
        httpOptions
      );
  }

}
