import { Component, OnInit } from '@angular/core';
import { Data } from '.././navigations'
import { NavigationsService } from '../../navigations/navigations.service'
import { Router,ActivatedRoute } from '@angular/router';
import { CustomMethodsService } from '../../../libraries/utils/custommethods.service';
import { AdviableVar } from '../../../libraries/utils/customvars';
import { Globals } from "../../main/globals";

@Component({
  selector: 'app-navigations_edit',
  templateUrl: './navigations.component.html',
  styleUrls: ['./navigations.component.css']
})
export class NavigationsEditComponent implements OnInit {
  model:Data;
  parent:number;
  constructor(  
    private custom_methods:CustomMethodsService,
    private router: Router,
    private route:ActivatedRoute, 
    private navigations:NavigationsService,
    private global:Globals) { }


  ngOnInit() {
      this.global.initializeActions();
      this.initialize();
      this.route.params.subscribe(params =>{

        if (typeof params['id'] !== 'undefined') 
        {       
          this.parent = params['id'];
          this.navigations.Get(params['id']).subscribe((data:Data) => {
          this.model = data;
          this.global.actions.is_new = false;
        });
        }
        
      });
    
  }
  initialize()
  {
    this.model =  {
      name:"",
      id:null,
      publish:true,
      checked:false,
      selected:false,
      navigation:null
    };
  }
  save()
  {  
      var that = this;
      var disable_updated_box = function() {
        that.global.actions.updated = false;
      }
      if (this.global.actions.is_new == false)
      {
        this.navigations.Update(this.model).subscribe((data:any) => {

          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }else
      {
        this.navigations.New(this.model).subscribe((data:Data) => {
          this.model = data;   
          this.global.actions.is_new = false;   
          this.global.actions.updated = true;
          setTimeout(disable_updated_box, 3000);
        });
      }
    
     
    
  }
  back()
  {
     this.router.navigate(['/navigations' ]);
  }
  DELETE()
  {
   
    this.navigations.Delete(this.model.id).subscribe((data:any) => {
      this.router.navigate(['/navigations' ]);

    });
    
  }

}
