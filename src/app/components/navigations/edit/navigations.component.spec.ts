import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigationsEditComponent } from './navigations.component';

describe('NavigationsEditComponent', () => {
  let component: NavigationsEditComponent;
  let fixture: ComponentFixture<NavigationsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavigationsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
