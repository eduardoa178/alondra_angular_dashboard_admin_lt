import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import { Headers } from '@angular/http';
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import { environment } from '../../../environments/environment';
import { Globals } from "../main/globals";
import { CookieService } from 'ngx-cookie-service';
import { HttpJsonHeaderOptionsService } from '../../libraries/utils/httpoptions.service';
import { Data,Pagination } from './navigations';

@Injectable({
  providedIn: 'root'
})
export class NavigationsService {

  
  constructor(
    private http: HttpClient, 
    private cookie:CookieService,
    private global:Globals, 
    private header: HttpJsonHeaderOptionsService) { }

  List(page) {
     
    const postedData = {page:page}
    return this.http
      .post( 
        environment.url + "navs/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  Search(page, query) {
    const postedData = {
      page:page,
      query:query
    }
    return this.http
      .post( 
        environment.url + "navs/search/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  New(postedData) {
   
    return this.http
      .post( 
        environment.url + "nav/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  Get(id) {
   
    const postedData = {
      id:id
    }    
    return this.http
      .post( 
        environment.url + "nav/details/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  Update(postedData) {
   
    return this.http
      .put( 
        environment.url + "nav/",
        postedData,
        this.header.get_auth(this.global.token)
      );
  }

  Delete(id:number) {

    const postedData = {
      id:id
    }  
    return this.http
      .delete( 
        environment.url + "nav/",
        this.header.get_auth_params_option(this.global.token,postedData)
      );
     
  }

}
