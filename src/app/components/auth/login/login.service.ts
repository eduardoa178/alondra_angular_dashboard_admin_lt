import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  
  constructor(private http: HttpClient) { }

  getUser(username, password) {
    const postedData = { 
      username: username, 
      password: password
    };
    return this
            .http
            .post( 
              environment.url + "get_auth_token/",
              postedData,
              httpOptions
            );
        }
}
