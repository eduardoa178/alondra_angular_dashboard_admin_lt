import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Globals } from "../../main/globals";
import { LoginService } from './login.service';

interface Data {
  token: string;
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	router: Router;

  username: string;
  password: string;
  remember_me: boolean;

  Globals : Globals ;

  constructor( router: Router,private loginService: LoginService,private globals:Globals) 
  {
  	this.router = router;
    this.remember_me = true;
    this.Globals = globals;
  }

  ngOnInit() 
  {
    if(this.Globals.get_token()==false)
    {
      this.router.navigate(['/dashboard']);
    }
  }
  Loginx()
  {


    this
      .loginService
      .getUser(this.username,this.password)
      .subscribe((data:Data) => {

        if (typeof data.token !== 'undefined') 
        {
     
          this.Globals.set_token(data.token, this.remember_me);
          this.router.navigate(['/dashboard']);
        }

      });
  	
  }
  Registerx()
  {
    this.router.navigate(['/newaccount']);
  }
}
