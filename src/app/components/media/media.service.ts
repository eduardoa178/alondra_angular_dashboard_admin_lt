import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Globals } from "../main/globals";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class MediaService {

  
  constructor(private http: HttpClient) { }

  List(page) {
    const postedData = {page:page}
    return this.http
      .post( 
        environment.url + "categories/",
        postedData,
        httpOptions
      );
  }

  Search(page, query) {
    const postedData = {
      page:page,
      query:query
    }
    return this.http
      .post( 
        environment.url + "categories/search/",
        postedData,
        httpOptions
      );
  }

  New(postedData) {
    return this.http
      .post( 
        environment.url + "categories/",
        postedData,
        httpOptions
      );
  }

  Get(id) {
    const postedData = {
      id:id
    }    
    return this.http
      .post( 
        environment.url + "category/details/",
        postedData,
        httpOptions
      );
  }

  Find(name) {
    const postedData = {
      name:name
    }    
    return this.http
      .post( 
        environment.url + "category/find/",
        postedData,
        httpOptions
      );
  }

  FindSlug(slug) {
    const postedData = {
      slug:slug
    }    
    return this.http
      .post( 
        environment.url + "category/find/slug/",
        postedData,
        httpOptions
      );
  }

  Update(postedData) {
      
    return this.http
      .post( 
        environment.url + "category/",
        postedData,
        httpOptions
      );
  }

  Delete(id) {
     const postedData = {
      id:id
    }   
    return this.http
      .post( 
        environment.url + "category/",
        postedData,
        httpOptions
      );
  }
}
