import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Globals } from "../main/globals";

@Component({
   selector: 'header',
  templateUrl: './header.component.html',
  //styleUrls: ['./app.component.css']
})
export class HeaderComponent implements OnInit {
	title = 'navigation';
	settings: boolean = true;
  router: Router;
  Globals : Globals ;
  ngOnInit() 
  {

  }
  constructor( router: Router,private globals:Globals) 
  {
  	this.router = router;
    this.Globals = globals;
  }

	Logoutx()
	{
   
    this.Globals.remove_token();
		this.router.navigate(['/login']);
  
	}
}
