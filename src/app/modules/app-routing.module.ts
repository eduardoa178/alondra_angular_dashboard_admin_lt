import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from '../components/main/app.component';

import { SigninupComponent } from '../components/auth/signinup/signinup.component';
import { LoginComponent } from '../components/auth/login/login.component';

import { DashboardComponent } from '../components/dashboard/dashboard.component';

import { PostComponent } from '../components/post/list/post.component';
import { PostEditComponent } from '../components/post/edit/post.component';

import { PageComponent } from '../components/page/list/page.component';
import { PageEditComponent } from '../components/page/edit/page.component'

import { MediaComponent } from '../components/media/list/media.component';
import { MediaEditComponent } from '../components/media/edit/media.component';

import { TagsComponent } from '../components/tags/list/tags.component';
import { TagsEditComponent } from '../components/tags/edit/tags.component';

import { UsersComponent } from '../components/users/list/users.component';
import { UsersEditComponent } from '../components/users/edit/users.component';

import { NavigationsComponent } from '../components/navigations/list/navigations.component';
import { NavigationsEditComponent } from '../components/navigations/edit/navigations.component';

import { NavigationsItemsComponent } from '../components/navigations_items/list/navigations_items.component';
import { NavigationsItemsEditComponent } from '../components/navigations_items/edit/navigations_items.component';

import { CommentsComponent } from '../components/comments/comments.component';
import { SettingsComponent } from '../components/settings/settings.component';

import { CategoriesComponent } from '../components/categories/list/categories.component';
import { CategoriesEditComponent } from '../components/categories/edit/categories.component';

import { PermissionComponent } from '../components/permissions/permissions/list/permissions.component';
import { PermissionsEditComponent } from '../components/permissions/permissions/edit/permissions.component';

import { GroupsComponent } from '../components/permissions/groups/list/groups.component';
import { GroupsEditComponent } from '../components/permissions/groups/edit/groups.component';

import { UserPermissionComponent } from '../components/permissions/user_permissions/list/user_permissions.component';
import { UserPermissionEditComponent } from '../components/permissions/user_permissions/edit/user_permissions.component';


const routes: Routes = [

  
  { path: 'dashboard', component: DashboardComponent },
  
  { path: 'newaccount', component: SigninupComponent },
  { path: 'login', component: LoginComponent },
 
  { path: 'posts', component: PostComponent },
  { path: 'posts/edit/:id', component: PostEditComponent },
  { path: 'posts/new', component: PostEditComponent },

  { path: 'pages', component: PageComponent },
  { path: 'pages/edit/:id', component: PageEditComponent },
  { path: 'pages/new', component: PageEditComponent },

  { path: 'permisions/groups', component: GroupsComponent },
  { path: 'permisions/groups/edit/:id', component: GroupsEditComponent },
  { path: 'permisions/groups/new', component: GroupsEditComponent },

  { path: 'permisions', component: PermissionComponent },
  { path: 'permisions/edit/:id', component: PermissionsEditComponent },
  { path: 'permisions/new', component: PermissionsEditComponent },

  //{ path: 'user/permisions-groups', component: UserPermissionComponent },
  { path: 'user/permisions-groups/:parent', component: UserPermissionEditComponent },


  { path: 'media', component: MediaComponent },
  { path: 'media/edit/:id', component: MediaEditComponent },
  { path: 'tags', component: TagsComponent },
  { path: 'tags/edit/:id', component: TagsEditComponent },
  { path: 'tags/new', component: TagsEditComponent },
  { path: 'users', component: UsersComponent },
  { path: 'users/edit/:id', component: UsersEditComponent },
  { path: 'users/new', component: UsersEditComponent },
  { path: 'categories', component: CategoriesComponent },
  { path: 'categories/new', component: CategoriesEditComponent },
  { path: 'categories/edit/:id', component: CategoriesEditComponent },
  { path: 'navigations', component: NavigationsComponent },
  { path: 'navigations/new', component: NavigationsEditComponent },
  { path: 'navigations/edit/:id', component: NavigationsEditComponent },
  { path: 'navigations/items/edit/:id/:parent', component: NavigationsItemsEditComponent },
  { path: 'navigations/items/new/:parent', component: NavigationsItemsEditComponent },
  { path: 'comments', component: CommentsComponent },
  { path: 'settings', component: SettingsComponent },
  { path:  '**', component: DashboardComponent },
  { path: '',   redirectTo: '/login', pathMatch: 'full'},

];

@NgModule({
  imports: [
    ///RouterModule.forRoot(routes) 
    RouterModule.forRoot(
      routes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule { }
