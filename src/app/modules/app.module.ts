import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';


import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HttpClient} from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

/// components
import { Globals } from '../components/main/globals';
import { AppComponent } from '../components/main/app.component';
import { NavigationComponent } from '../components/navigation/navigation.component';
import { HeaderComponent } from '../components/header/header.component';
import { SidebarComponent } from '../components/sidebar/sidebar.component';

import { SigninupComponent } from '../components/auth/signinup/signinup.component';

import { LoginComponent } from '../components/auth/login/login.component';
import { LoginService } from '../components/auth/login/login.service';

import { DashboardComponent } from '../components/dashboard/dashboard.component';
import { DashboardService } from '../components/dashboard/dashboard.service';

import { PostComponent } from '../components/post/list/post.component';
import { PostEditComponent } from '../components/post/edit/post.component';
import { PostSelectableComponent } from '../components/post/selectable/post.component';

import { PostService } from '../components/post/post.service';

import { PageComponent } from '../components/page/list/page.component';
import { PageEditComponent } from '../components/page/edit/page.component'
import { PageService } from '../components/page/page.service';

import { MediaComponent } from '../components/media/list/media.component';
import { MediaEditComponent } from '../components/media/edit/media.component';
import { MediaService } from '../components/media/media.service';

import { TagsComponent } from '../components/tags/list/tags.component';
import { TagsEditComponent } from '../components/tags/edit/tags.component';
import { TagsSelectableComponent } from '../components/tags/selectable/tags.component';
import { TagsService } from '../components/tags/tags.service';


import { UsersComponent } from '../components/users/list/users.component';
import { UsersEditComponent } from '../components/users/edit/users.component';
import { UsersService } from '../components/users/users.service';

import { NavigationsComponent } from '../components/navigations/list/navigations.component';
import { NavigationsEditComponent } from '../components/navigations/edit/navigations.component';
import { NavigationsService } from '../components/navigations/navigations.service';

import { CommentsComponent } from '../components/comments/comments.component';
import { CommentsService } from '../components/comments/comments.service';

import { SettingsComponent } from '../components/settings/settings.component';
import { SettingsService } from '../components/settings/settings.service';

import { CategoriesComponent } from '../components/categories/list/categories.component';
import { CategoriesEditComponent } from '../components/categories/edit/categories.component';
import { CategoryService } from '../components/categories/category.service';

import { CategoriesSelectableComponent } from '../components/categories/selectable/categories.component';

import { NavigationsItemsComponent } from '../components/navigations_items/list/navigations_items.component';
import { NavigationsItemsEditComponent } from '../components/navigations_items/edit/navigations_items.component';
import { NavigationsItemsSelectableComponent } from '../components/navigations_items/selectable/navigations_items.component';

import { PermissionComponent } from '../components/permissions/permissions/list/permissions.component';
import { PermissionsEditComponent } from '../components/permissions/permissions/edit/permissions.component';

import { GroupsComponent } from '../components/permissions/groups/list/groups.component';
import { GroupsEditComponent } from '../components/permissions/groups/edit/groups.component';

import { UserPermissionComponent } from '../components/permissions/user_permissions/list/user_permissions.component';
import { UserPermissionEditComponent } from '../components/permissions/user_permissions/edit/user_permissions.component';
//libraries

import { HttpJsonHeaderOptionsService } from '../libraries/utils/httpoptions.service';
import { CustomMethodsService } from '../libraries/utils/custommethods.service';
import { MDEditorComponent } from '../libraries/utils/mdeditor';





export function HttpLoaderFactory(http: HttpClient) 
{
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
  
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    DashboardComponent,
    NavigationComponent,

    SettingsComponent,

    SigninupComponent,

    LoginComponent,

    PostComponent,
    PostEditComponent,
    PostSelectableComponent,
    
    PageComponent,
    PageEditComponent,

    MediaComponent,
    MediaEditComponent,

    TagsComponent,
    TagsEditComponent,
    TagsSelectableComponent,

    UsersComponent,
    UsersEditComponent,

    NavigationsComponent,
    NavigationsEditComponent,
    
    NavigationsItemsComponent,
    NavigationsItemsEditComponent,
    NavigationsItemsSelectableComponent,

    CommentsComponent,

    CategoriesComponent,
    CategoriesSelectableComponent,
    CategoriesEditComponent,

    PermissionComponent,
    PermissionsEditComponent,

    GroupsComponent,
    GroupsEditComponent,

    UserPermissionComponent,
    UserPermissionEditComponent,
    //libraries
    MDEditorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
  ],
  providers: [
      CookieService,
      Globals,
      LoginService,
      DashboardService,
      PostService,
      PageService,
      MediaService,
      TagsService,
      UsersService,
      NavigationsService,
      CommentsService,
      SettingsService,
      CategoryService,
      //vendor
      HttpJsonHeaderOptionsService,
      CustomMethodsService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
